import util.Configuration;
import util.Order;
import util.StopCondition;
import util.StopException;

import java.util.Collections;

public class Main {


    public static void main(String[] args) {
        System.out.println(new Main().fullIteration(StopCondition.AMOUNT_OF_LOOPS));
    }

    /*метод поочередного варьирования*/
    public double variableVariation(StopCondition condition) {
        Configuration cfg = new Configuration(Collections.singleton(condition));
        double funcValueMin = Double.MAX_VALUE; //cause extreme is minimum

        try {
             /*нахождение околоэкстремальной области путем поочередного изменения координат*/
            for (; ; cfg.toggleLoopsAmount()) {
                if (y1Bounds((float) cfg.y1, cfg) && y2Bounds((float) cfg.y2, cfg)) break;
                switch (cfg.order) {
                    case Y1:
                        if (cfg.y1 < cfg.y2Min) {
                            cfg.y1 += cfg.step1;
                        }
                        break;
                    case Y2:
                        if (cfg.y2 > cfg.y1Max) {
                            cfg.y2 -= cfg.step2;
                        }
                        break;
                }
                toggleOrder(cfg);
            }

            for (; ; cfg.toggleLoopsAmount()) {

                if (y1Bounds((float) cfg.y1, cfg) && y2Bounds((float) cfg.y2, cfg)) { //variables in bounds
                    double val = func(cfg.y1, cfg.y2, cfg); //function value
                    if (val < funcValueMin) {
                        cfg.toggleAccuracy(val, funcValueMin);
                        funcValueMin = val; //good direction
                        switch (cfg.order) {
                            case Y1:
                                cfg.y1 += cfg.step1;
                                break;
                            case Y2:
                                cfg.y2 -= cfg.step1;
                                break;
                        }
                    } else {
                        //go back
                        cfg.y1 -= cfg.step1;
                        cfg.y2 -= cfg.step1;
                        cfg.step1 /= 2;
                        cfg.step2 /= 2;
                    }
                    toggleOrder(cfg);
                } else {
                    break;
                }
            }
        } catch (StopException e) {
            e.printStackTrace();
        }

        return funcValueMin;
    }

    /*метод наискорейшего спуска*/
    private double fastDown(StopCondition condition) {

        Configuration cfg = new Configuration(Collections.singleton(condition));

        /*вычисление значений частных производных*/
        double dfY1 = dfY1(cfg.y1, cfg.y2, cfg);
        double dfY2 = dfY2(cfg.y1, cfg.y2, cfg);

        /*вычисление первоначального значения функции*/
        double prevVal = func(cfg.y1, cfg.y2, cfg);
        double funcValueMin = Double.MAX_VALUE;

        try {
            /*ортогональное движение до кратчайшей координаты околоэкстремальной области y1 или y2 */
            for (; ; cfg.toggleLoopsAmount()) {

                /*выход из цикла при достижении околоэкстремальной области */
                if (y1Bounds((float) cfg.y1, cfg) && y2Bounds((float) cfg.y2, cfg)) {
                    break;
                }

                double val = func(cfg.y1, cfg.y2, cfg);
                if (val > prevVal) {
                    /*смена направления движения*/
                    dfY1 = dfY1(cfg.y1, cfg.y2, cfg);
                    dfY2 = dfY2(cfg.y1, cfg.y2, cfg);
                    prevVal = func(cfg.y1, cfg.y2, cfg);
                    continue;
                } else {
                    prevVal = val;
                }

                if (dfY1 > dfY2) {
                    cfg.y1 += cfg.step1;
                    cfg.order = Order.Y1; //порядок для дальнейшего движения в околоэкстремальной области
                } else {
                    cfg.y2 -= cfg.step2;
                    cfg.order = Order.Y2; //порядок для дальнейшего движения в околоэкстремальной области
                }

            }

            /*нахождение экстремумума с полученным направлением путем половинного деления*/
            for (; ; cfg.toggleLoopsAmount()) {

                if (!y1Bounds((float) cfg.y1, cfg) || !y2Bounds((float) cfg.y2, cfg)) {
                    break; //вышли за границы области
                }

                double val = func(cfg.y1, cfg.y2, cfg);
                if (val < funcValueMin) { //все хорошо
                    switch (cfg.order) {
                        case Y1:
                            cfg.y1 += cfg.step1;
                            break;
                        case Y2:
                            cfg.y2 -= cfg.step2;
                            break;
                    }
                    funcValueMin = val;
                } else {
                    switch (cfg.order) {
                        case Y1:
                            cfg.y1 -= (cfg.step1 * 2); //на 2 шага назад
                            cfg.step1 /= 2; //уменьшаем размер шага
                            break;
                        case Y2:
                            cfg.y2 += (cfg.step2 * 2);
                            cfg.step2 /= 2;
                            break;
                    }
                }
            }
        } catch (StopException e) {
            e.printStackTrace();
        }

        return funcValueMin;
    }

    /*метод полного перебора*/
    private double fullIteration(StopCondition condition) {
        // FIXME: 12/24/17 init condition

        Configuration cfg = new Configuration(Collections.singleton(condition));

        double funcValueMin = Double.MAX_VALUE;
        int iterationsY1 = (int) ((cfg.y1Max - cfg.y1Min) / cfg.step1);
        int iterationsY2 = (int) ((cfg.y2Max - cfg.y2Min) / cfg.step1);
        int iMax = 0;
        int jMax = 0;

        try {
            for (int i = 0; i < iterationsY1; i++, cfg.toggleLoopsAmount()) {
                for (int j = i % 2 == 0 ? 0 : iterationsY2 - 1;
                     i % 2 == 0 ? j < iterationsY2 : j >= 0;
                     j = i % 2 == 0 ? j + 1 : j - 1, cfg.toggleLoopsAmount()) {
                    double y1Val = cfg.y1Min + i * cfg.step1;
                    double y2Val = cfg.y2Min + j * cfg.step2;
                    double val = func(y1Val, y2Val, cfg);
                    if (val < funcValueMin) {
                        iMax = i;
                        jMax = j;
                        funcValueMin = val;
                    }
                }
            }

            double y2Val = cfg.y2Min + jMax * cfg.step2;

            for (; ; cfg.toggleLoopsAmount()) {
                // FIXME: 12/24/17 if point out of bounds
                cfg.y1Min = cfg.y1Min + (iMax - 1) * cfg.step1;
                cfg.y1Max = cfg.y1Min + cfg.step1 * 2;
                cfg.step1 /= 2;
                iterationsY1 = (int) ((cfg.y1Max - cfg.y1Min) / cfg.step1);

                for (int i = 0; i < iterationsY1; i++, cfg.toggleCalcAmount()) {
                    double y1Val = cfg.y1Min + i * cfg.step1;
                    double val = func(y1Val, y2Val, cfg);
                    if (val < funcValueMin) {
                        cfg.toggleAccuracy(funcValueMin, val);
                        funcValueMin = val;
                    }
                }

            }


        } catch (StopException e) {
            e.printStackTrace();
        }


        return funcValueMin;

    }

    private void toggleOrder(Configuration cfg) {
        cfg.order = cfg.order == Order.Y1 ? Order.Y2 : Order.Y1;
    }

    private boolean y1Bounds(float y1, Configuration cfg) {
        return y1 >= cfg.y1Min && y1 <= cfg.y1Max;
    }

    private boolean y2Bounds(float y2, Configuration cfg) {
        return y2 >= cfg.y2Min && y2 <= cfg.y2Max;
    }

    private double func(double y1, double y2, Configuration cfg) {
        cfg.toggleCalcAmount();
        return Math.pow(y2 - 1, 2) + Math.pow(Math.exp(y1 + y2), 2) - y1 + y2;
    }

    private double dfY1(double y1, double y2, Configuration cfg) {
        cfg.toggleCalcAmount();
        return Math.pow(Math.exp(y1 + y2), 2) - 1;
    }

    private double dfY2(double y1, double y2, Configuration cfg) {
        cfg.toggleCalcAmount();
        return Math.pow(Math.exp(y1 + y2), 2) + 2 * y2 - 1;
    }

}
