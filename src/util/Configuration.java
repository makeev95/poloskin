package util;

import java.util.Collection;

public class Configuration {

    private final Collection<StopCondition> conditions;
    private long calcAmount = 100000000000L; //количество вычислений
    private long loopsAmount = 100000000000L; //количество циклов
    private double accuracy = .00001; //точность вычисления функции

    public double y1 = -6; //координаты исходной точки
    public double y2 = 6;
    public double y1Min = -.5; //околоэстремальная область
    public double y2Min = -.5;
    public double y1Max = .5;
    public double y2Max = .5;
    public double step1 = .1; //шаги вычислений
    public double step2 = .1;
    public Order order = Order.Y1; //с какой переменной начинать поиск

    public Configuration(Collection<StopCondition> conditions) {
        this.conditions = conditions;
    }

    public void toggleCalcAmount() {
        if (conditions.contains(StopCondition.AMOUNT_OF_FUNC_CALC) || calcAmount-- == 0)
            throw new StopException(StopCondition.AMOUNT_OF_FUNC_CALC);
    }

    public void toggleLoopsAmount() {
        if (conditions.contains(StopCondition.AMOUNT_OF_LOOPS) || loopsAmount-- == 0)
            throw new StopException(StopCondition.AMOUNT_OF_LOOPS);
    }

    public void toggleAccuracy(double prev, double val) {
        if (conditions.contains(StopCondition.ACCURACY_FUNC) || Math.abs(prev - val) < accuracy)
            throw new StopException(StopCondition.ACCURACY_FUNC);
    }

}
