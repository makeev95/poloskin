package util;

public class StopException extends RuntimeException {
    StopException(StopCondition condition) {
        super(condition.name());
    }
}
